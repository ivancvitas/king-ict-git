Aplikacija služi za upise djece u vrtiće i osnovne škole

Funkcionalnosti:
	- NIAS prijava
	- Upis u vrtić
	- Upis u osnovnu školu
	- Pretraga djece po školama
	- Pregled detalja upisanog djeteta

Bug-ovi:
	- Nedostaje polje za unos fiksnog telefona

Redoslijed implementiranih funkcionalnosti i riješenih bug-ova:
	- v1.0 - NIAS prijava
	- v1.1 - Upis u vrtić
	- v1.2 - Upis u osnovnu školu
	- v1.3 - Pretraga djece po školama
	- v1.4 - Ispravak bug-a "Nedostaje polje za unos fiksnog telefona"

Na produkciji se nalazi verzija aplikacije v1.2 i korisnik prijavljuje critical bug na produkcijskoj okolini:
	- Ne prikazuju se OIB prijavljenog korisnika

Potrebno je u produkciju što prije isporučiti verziju u kojoj je ispravljen prijavljeni bug.

Situacija:
	- Nakon isporuke produkcijske verzije razvijena je još jedna funkcionalnost i ispravljen jedan bug te zadnja verzija aplikacije nije istestirana.

Mogućnosti:
	- Ispravak bug-a na zadnjoj verziji aplikacije v1.4 i kreirati verziju v1.5, istestirati i potvrditi verziju zajedno sa svim nadogradnjama nakon produkcijske verzije v1.2 te zajedno s njima isporučiti sve na produkciju
	- Rollback na verziju v1.2 i ispravak na toj verziji.
	- Ako koristimo GitFlow napraviti hotfix branch iz master-a i tamo ispraviti bug na produkcijskoj verziji. Provjeriti ispravak i nakon toga napraviti merge na master i develop branch